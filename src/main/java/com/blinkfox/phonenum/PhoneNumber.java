package com.blinkfox.phonenum;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 电话号码外排序类.
 * Created by blinkfox on 2017/5/7.
 */
public class PhoneNumber {

    /* 临时文件最大行数 */
    private static final int MAX_LINE = 1500000;

    /* 输出目录 */
    private static String tempDirectory = "";

    /* 存放临时文件的集合 */
    private static final List<File> tempFiles = new ArrayList<File>();

    private static final Log log = Log.get(PhoneNumber.class);

    static {
        // 初始化生成临时文件目录
        FileUtils.initTempDirectory();
    }

    /**
     * main方法.
     * @param args 数组参数
     * @throws IOException 异常
     */
    public static void main(String[] args) throws IOException {
        if (!checkParams(args)) {
            return;
        }
        final long startTime = System.currentTimeMillis();
        log.info("排序开始...");

        // 先拆分原始的大文件为各个有序的小文件，最后使用归并排序的实现将各个小文件合并成一个有序的大文件.
        splitFiles(args[0]);
        log.info("拆分成小文件结束，开始合并大文件,拆分耗时:" + (System.currentTimeMillis() - startTime) + " ms");

        long startTime1 = System.currentTimeMillis();
        //mergeFiles(args[1]);
        log.info("合并成大文件结束,合并文件耗时:" + (System.currentTimeMillis() - startTime1) + " ms");

        log.info("最终执行成功！,所有执行耗时:" + (System.currentTimeMillis() - startTime) + " ms");
        FileUtils.cleanTempDirectory();
    }

    /**
     * 检查传入的数组参数是否合法.
     * @param args 数组参数
     */
    private static boolean checkParams(String[] args) {
        if (args == null || args.length < 2 || args[0] == null || args[1] == null) {
            log.warn("未检测到合法的输入文件和输出文件全路径名的参数...");
            return false;
        }

        if (args[0].equals(args[1])) {
            log.warn("输入文件和输出文件不能相同...");
            return false;
        }
        return true;
    }

    /**
     * 拆分大文件为各个小文件.
     */
    private static void splitFiles(String inputFileName) {
        InputStream is = null;
        BufferedReader reader = null;
        try {
            is = new FileInputStream(inputFileName);
            reader = new BufferedReader(new InputStreamReader(is));
            List<Long> phoneNums = new ArrayList<Long>();

            // 读取代码行数，并拆分成不同的数据输出到多个临时文件中
            int currLines = 0;
            String line;
            while ((line = reader.readLine()) != null) {
                currLines += 1;
                phoneNums.add(Long.parseLong(line));

                // 如果当前读取的行数大于了最大行数，则排序目前的集合，且输出到临时文件中
                if (currLines >= MAX_LINE) {
                    sortAndOutputFile(phoneNums);
                    currLines = 0;
                    phoneNums.clear();
                }
            }

            // 排序和输出剩余的电话号码
            if (phoneNums.size() > 0) {
                sortAndOutputFile(phoneNums);
                phoneNums.clear();
            }
        } catch (IOException e) {
            log.error("拆分大文件为各个小文件出错！", e);
        } finally {
            FileUtils.closeQuietly(is);
            FileUtils.closeQuietly(reader);
        }
    }

    /**
     * 合并多个有序的文件为一个有序的大文件.
     */
    private static void mergeFiles(String outputFileName) {
        List<Reader> readers = new ArrayList<Reader>();
        Map<Long, BufferedReader> readerMap = new HashMap<Long, BufferedReader>();

        FileWriter fileWriter = null;
        BufferedWriter writer = null;
        try {
            // 将各个小文件的基本信息初始化到Reader集合和Map中，供接下来的排序时使用
            for (File tempFile : tempFiles) {
                // 生成各临时文件的FileReader并存到集合中，这样好在最后关闭所有的FileReader
                FileReader fileReader = new FileReader(tempFile);
                readers.add(fileReader);

                // 生成BufferedReader并存到HashMap中，供接下来好存、取Map中的R数据
                BufferedReader reader = new BufferedReader(fileReader);
                String phoneNum = reader.readLine();
                if (phoneNum != null) {
                    readerMap.put(Long.parseLong(phoneNum), reader);
                }
            }

            // 采用归并排序的思想,一直循环排序直到各个文件没有了电话号码为止
            fileWriter = new FileWriter(outputFileName);
            writer = new BufferedWriter(fileWriter);
            List<Long> phoneNums = new LinkedList<Long>(readerMap.keySet());
            while (readerMap.size() > 0) {
                // 排序，将最大的手机号从phoneNums集合中移除，并写入到输出文件中
                Collections.sort(phoneNums, LongComparator.getInstance());
                Long phoneNum = phoneNums.remove(0);
                writer.append(toNumStr(phoneNum)).append('\n');

                // 从readerMap中移除刚才得出的最大手机号,并从该reader中读取下一个手机号存到List和Map中，继续循环写入
                BufferedReader reader = readerMap.remove(phoneNum);
                String nextPhoneNumStr = reader.readLine();
                if (nextPhoneNumStr != null) {
                    Long nextPhoneNum = Long.parseLong(nextPhoneNumStr);
                    phoneNums.add(nextPhoneNum);
                    readerMap.put(nextPhoneNum, reader);
                }
            }
        } catch (IOException e) {
            log.error("合并多个有序的小文件为一个有序的大文件出错！", e);
        } finally {
            FileUtils.closeQuietly(readers);
            FileUtils.closeQuietly(writer);
            FileUtils.closeQuietly(fileWriter);
        }
    }

    /**
     * 排序集合数据，并输出到临时文件中文件.
     * @param phoneNums 电话号码集合
     */
    private static void sortAndOutputFile(List<Long> phoneNums) {
        long startTime = System.currentTimeMillis();
        Collections.sort(phoneNums, LongComparator.getInstance());
        log.info("排序集合耗时:" + (System.currentTimeMillis() - startTime) + " ms");

        // 输出到临时文件
        long startTime1 = System.currentTimeMillis();
        File file = new File(FileUtils.buildOutputFileName());
        tempFiles.add(file);
        FileUtils.writeLines(file, phoneNums);
        log.info("生成了一个临时文件,生成耗时:" + (System.currentTimeMillis() - startTime1) + " ms");
    }

    /**
     * 将long型数据转换成电话号码的11位字符串.
     * @param l 长整型数字
     * @return 字符串
     */
    private static String toNumStr(Long l) {
        String str = l.toString();
        int len;
        if ((len = str.length()) == 11) {
            return str;
        }

        // 长度不是11位则在后面补0
        StringBuilder sb = new StringBuilder(str);
        for (int i = 0; i < 11 - len; i++) {
            sb.append("0");
        }
        return sb.toString();
    }

    /**
     * Long长整型数据集合的倒序排序的单例类.
     */
    private static class LongComparator implements Comparator<Long> {

        /* 初始化不可变实例 */
        private static final LongComparator comparator = new LongComparator();

        /**
         * 私有构造方法，防止 new 新的实例.
         */
        private LongComparator() {
            super();
        }

        /**
         * 获取唯一实例.
         * @return Comparator实例
         */
        private static LongComparator getInstance() {
            return comparator;
        }

        /**
         * 反向比较，实现倒序.
         * @param o1 o1对象
         * @param o2 o2对象
         * @return int
         */
        public int compare(Long o1, Long o2) {
            return o2.compareTo(o1);
        }

    }

    /**
     * 输入输出流、文件等操作的工具类.
     */
    private static class FileUtils {

        private FileUtils() {
            super();
        }

        /**
         * 构建输出的临时文件全路径名.
         * @return 文件全路径名字符串
         */
        private static String buildOutputFileName() {
            return new StringBuilder(80).append(tempDirectory).append(File.separator)
                    .append(System.currentTimeMillis()).append("_temp.txt").toString();
        }

        /**
         * 将集合中的数据输出到指定文件名的文件中.
         * @param file  文件
         * @param lines 集合
         */
        private static void writeLines(File file, List<Long> lines) {
            if (lines == null || lines.isEmpty()) {
                return;
            }

            FileWriter fileWriter = null;
            BufferedWriter writer = null;
            try {
                // 写入集合中的数据文本文件中，一个元素一行，
                fileWriter = new FileWriter(file);
                writer = new BufferedWriter(fileWriter);
                for (Long line : lines) {
                    if (line != null) {
                        writer.append(toNumStr(line)).append('\n');
                    }
                }
            } catch (IOException e) {
                log.error("生成输出流出错！", e);
            } finally {
                FileUtils.closeQuietly(writer);
                FileUtils.closeQuietly(fileWriter);
            }
        }

        /**
         * 初始化用于生成临时文件的目录.
         */
        private static void initTempDirectory() {
            tempDirectory = new StringBuilder(60).append(System.getProperty("user.home"))
                    .append(File.separator).append("phonenum_temp").toString();
            File file = new File(tempDirectory);
            file.mkdirs();
            if (!file.exists() || !file.isDirectory()) {
                log.error("创建临时目录失败!");
                throw new IllegalArgumentException("创建临时目录失败!");
            }
        }

        /**
         * 删除临时目录及其中的内容.
         */
        private static void cleanTempDirectory() {
            if (deleteDirFiles(tempDirectory)) {
                log.info("清除了生成的临时目录和临时文件!");
            } else {
                log.warn("删除临时目录和其下的文件失败!");
            }
        }

        /**
         * 删除临时目录及其中的内容.
         * @param dir 目录全路径
         * @return 是否成功
         */
        private static boolean deleteDirFiles(String dir) {
            File file = new File(dir);
            if (file.isDirectory()) {
                // 文件夹目录列表
                String[] childs = file.list();
                if (childs != null) {
                    for (String child : childs) {
                        boolean success = deleteDirFiles(dir + File.separator + child);
                        if (!success) {
                            return false;
                        }
                    }
                }
            }
            return file.delete();
        }

        /**
         * 关闭Reader.
         * @param readers readers
         */
        private static void closeQuietly(List<Reader> readers) {
            if (readers != null && !readers.isEmpty()) {
                try {
                    for (Reader reader : readers) {
                        reader.close();
                    }
                } catch (IOException e) {
                    log.error("关闭readers的集合出错！", e);
                }
            }
        }

        /**
         * 关闭Reader.
         * @param reader reader
         */
        private static void closeQuietly(Reader reader) {
            closeQuietly(Arrays.asList(reader));
        }

        /**
         * 关闭输入流.
         * @param input 输入流
         */
        private static void closeQuietly(InputStream input) {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                log.error("关闭input输入流出错！", e);
            }
        }

        /**
         * 关闭writer.
         * @param writer writer
         */
        private static void closeQuietly(Writer writer) {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                log.error("关闭writer出错！", e);
            }
        }

    }

    /**
     * 自定义日志输出类.
     */
    private static class Log {

        // jdk 自带的 logger.
        private Logger logger;

        /**
         * 私有构造方法.
         * @param logger jdk Logger对象
         */
        private Log(Logger logger) {
            this.logger = logger;
        }

        /**
         * 构造并获取日志对象的实例.
         * @param cls 记录日志的类Class
         * @return 返回自身的实例
         */
        private static Log get(Class<?> cls) {
            return new Log(Logger.getLogger(cls.getName()));
        }

        /**
         * 记录 info 级别的日志信息.
         * @param msg 日志消息
         */
        private void info(String msg) {
            logger.info(msg);
        }

        /**
         * 记录 warning 级别的日志信息.
         * @param msg 日志消息
         */
        private void warn(String msg) {
            logger.warning(msg);
        }

        /**
         * 记录 error 级别的日志信息和异常信息.
         * @param msg 日志消息
         */
        private void error(String msg) {
            logger.log(Level.SEVERE, msg);
        }

        /**
         * 记录 error 级别的日志信息和异常信息.
         * @param msg 日志消息
         * @param t Throwable对象
         */
        private void error(String msg, Throwable t) {
            logger.log(Level.SEVERE, msg, t);
        }

    }

}