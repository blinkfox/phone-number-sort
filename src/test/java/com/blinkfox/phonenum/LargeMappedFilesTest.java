package com.blinkfox.phonenum;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import org.junit.Test;

/**
 * LargeMappedFiles单元测试类.
 * Created by blinkfox on 2017/5/9.
 */
public class LargeMappedFilesTest {

    // 64 MB的文件
    private static final int LENGTH = 0x8000000;

    private static final String FILE_NAME = "/Users/blinkfox/Downloads/fc_test.txt";

    /**
     * 测试读写文件内容.
     */
    @Test
    public void test() throws IOException {
        final long startTime = System.currentTimeMillis();

        FileChannel channel = new FileOutputStream(FILE_NAME).getChannel();
        MappedByteBuffer out = channel.map(FileChannel.MapMode.READ_WRITE, 0, LENGTH);

        // 向文件中写入64M内容的
        for (int i = 0; i < LENGTH; i++) {
            out.put((byte) 'x');
        }
        System.out.println("写入文件内容完成!");

        // 读取文件中间6个字节的内容
        int mid = LENGTH / 2;
        for (int i = mid; i < mid + 6; i++) {
            System.out.println("char:" + out.get(i));
        }
        channel.close();

        System.out.println("最终执行成功！,所有执行耗时:" + (System.currentTimeMillis() - startTime) + " ms");
    }

}