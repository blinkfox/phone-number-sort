package com.blinkfox.phonenum;

import org.junit.Test;

import java.io.IOException;

/**
 * PhoneNumber单元测试类. Created by blinkfox on 2017/5/7.
 */
public class PhoneNumberTest {

    /* 输入、输出文件 */
    // private static final String INPUT_FILE_NAME = "H:\\programs\\phonenum\\phonenum.txt";
    // private static final String OUTPUT_FILE_NAME = "H:\\programs\\phonenum\\phonenum_sorted.txt";
    private static final String INPUT_FILE_NAME = "/Users/blinkfox/Downloads/phonenum.txt";
    private static final String OUTPUT_FILE_NAME = "/Users/blinkfox/Downloads/phonenum_sorted.txt";

    /**
     * 测试主方法.
     */
    @Test
    public void testMain() throws IOException {
        PhoneNumber.main(new String[]{INPUT_FILE_NAME, OUTPUT_FILE_NAME});
    }

}